﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//ГАММИРОВАНИЕ: ШИФРОВКА/РАСШИФРОВКА///////////////////////////////////////////////////////
/*
 Поле. Старая, покривившаяся, давно заброшенная часовенка, возле нее колодец, большие камни, когда-то бывшие, по-видимому, могильными плитами, и старая скамья. Видна дорога в усадьбу Гаева. В стороне, возвышаясь, темнеют тополи: там начинается вишневый сад. Вдали ряд телеграфных столбов, и далеко-далеко на горизонте неясно обозначается большой город, который бывает виден только в очень хорошую, ясную погоду. Скоро сядет солнце. Шарлотта, Яша и Дуняша сидят на скамье; Епиходов стоит возле и играет на гитаре; все сидят задумавшись. Шарлотта в старой фуражке; она сняла с плеч ружье и поправляет пряжку на ремне.
     */
namespace Task4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            BtnReverse.Content = "<--";
        }

        private void BtnEncrypt_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Поле ключа не заполнено", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }

            TbOutput.Text = "";
            for (int i = 0; i < TbInput.Text.Length; i++)
            {
                TbOutput.Text += (char)((char)(TbKey.Text[i % TbKey.Text.Length])^(char)(TbInput.Text[i]));
            }
        }

        private void BtnReverse_Click(object sender, RoutedEventArgs e)
        {
            TbInput.Text = TbOutput.Text;
            TbOutput.Text = "";
        }
    }
}
//ГАММИРОВАНИЕ: ШИФРОВКА/РАСШИФРОВКА///////////////////////////////////////////////////////