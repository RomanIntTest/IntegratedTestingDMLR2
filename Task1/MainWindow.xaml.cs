﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//ШИФР ЦЕЗАРЯ: ШИФРОВКА И РАСШИФРОВКА/////////////////////////////////////////////////////////////////////////////////
/*
 Поле. Старая, покривившаяся, давно заброшенная часовенка, возле нее колодец, большие камни, когда-то бывшие, по-видимому, могильными плитами, и старая скамья. Видна дорога в усадьбу Гаева. В стороне, возвышаясь, темнеют тополи: там начинается вишневый сад. Вдали ряд телеграфных столбов, и далеко-далеко на горизонте неясно обозначается большой город, который бывает виден только в очень хорошую, ясную погоду. Скоро сядет солнце. Шарлотта, Яша и Дуняша сидят на скамье; Епиходов стоит возле и играет на гитаре; все сидят задумавшись. Шарлотта в старой фуражке; она сняла с плеч ружье и поправляет пряжку на ремне.
     */
namespace Task1a
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            BtnReverse.Content = "<--";
        }

        private void BtnEncrypt_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length==0 || TbKey.Text.Length > 2)
            {
                System.Windows.MessageBox.Show("Неверная длина ключа. Ключ должен содержать 1 или 2 цифры", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            if ((TbKey.Text.Length == 1 && !Char.IsDigit(TbKey.Text[0])) || (TbKey.Text.Length == 2 && (!Char.IsDigit(TbKey.Text[0]) || !Char.IsDigit(TbKey.Text[1]))))
            {
                System.Windows.MessageBox.Show("Неверный формат ключа. Ключ должен содержать только цифры", "Ошибка");
                return;
            }
            if (Int32.Parse(TbKey.Text)>32)
            {
                System.Windows.MessageBox.Show("Неверная величина сдвига. Ключ должен быть в диапазоне от 0 до 32", "Ошибка");
                return;
            }
            TbOutput.Text = "";
            int Shift = Int32.Parse(TbKey.Text);
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            string InputText = TbInput.Text.ToLower();
            for (int i = 0; i<InputText.Length; i++)
            {
                if (Alphabet.Contains(InputText[i]))
                    TbOutput.Text += Alphabet[(Alphabet.IndexOf(InputText[i]) + Shift) % 33];
            }
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }

        private void BtnDecrypt_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0 || TbKey.Text.Length > 2)
            {
                System.Windows.MessageBox.Show("Ошибка", "Неверная длина ключа. Ключ должен содержать 1 или 2 цифры");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            if ((TbKey.Text.Length == 1 && !Char.IsDigit(TbKey.Text[0])) || (TbKey.Text.Length == 2 && (!Char.IsDigit(TbKey.Text[0]) || !Char.IsDigit(TbKey.Text[1]))))
            {
                System.Windows.MessageBox.Show("Ошибка", "Неверный формат ключа. Ключ должен содержать только цифры");
                return;
            }
            if (Int32.Parse(TbKey.Text) > 32)
            {
                System.Windows.MessageBox.Show("Ошибка", "Неверная величина сдвига. Ключ должен быть в диапазоне от 0 до 32");
                return;
            }
            TbOutput.Text = "";
            int Shift = Int32.Parse(TbKey.Text);
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            string InputText = TbInput.Text.ToLower();
            for (int i = 0; i < InputText.Length; i++)
            {
                if (Alphabet.Contains(InputText[i]))
                    TbOutput.Text += Alphabet[(Alphabet.IndexOf(InputText[i]) - Shift +33) % 33];
            }
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }

        private void BtnReverse_Click(object sender, RoutedEventArgs e)
        {
            TbInput.Text = TbOutput.Text;
            TbOutput.Text = "";
        }
    }
}
//ШИФР ЦЕЗАРЯ: ШИФРОВКА И РАСШИФРОВКА/////////////////////////////////////////////////////////////////////////////////