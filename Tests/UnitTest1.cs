﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EncriptSystem;

namespace Tests
{
    [TestClass]
    public class Caesar
    {
        [TestMethod]
        public void EncriptCaesarTest1()
        {
            //arrange
            string input = "аааа";
            string key = "3";
            string expected = "гггг";
            //act
            string result = CryptographyTechniques.EncriptCaesar(input,key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptCaesarTest2()
        {
            //arrange
            string input = "//..,,.";
            string key = "1";
            string expected = "";
            //act
            string result = CryptographyTechniques.EncriptCaesar(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptCaesarTest3()
        {
            //arrange
            string input = "выаь";
            string key = "к";
            string expected = null;
            //act
            string result = CryptographyTechniques.EncriptCaesar(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptCaesarTest4()
        {
            //arrange
            string input = "";
            string key = "";
            string expected = null;
            //act
            string result = CryptographyTechniques.EncriptCaesar(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptCaesarTest1()
        {
            //arrange
            string input = "гггг";
            string key = "3";
            string expected = "аааа";
            //act
            string result = CryptographyTechniques.DecriptCaesar(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptCaesarTest2()
        {
            //arrange
            string input = "//..,,.";
            string key = "1";
            string expected = "";
            //act
            string result = CryptographyTechniques.DecriptCaesar(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptCaesarTest3()
        {
            //arrange
            string input = "выаь";
            string key = "к";
            string expected = null;
            //act
            string result = CryptographyTechniques.DecriptCaesar(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptCaesarTest4()
        {
            //arrange
            string input = "";
            string key = "";
            string expected = null;
            //act
            string result = CryptographyTechniques.DecriptCaesar(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CrackCaesarTest1()
        {
            //arrange
            string input = "п";
            string expected = "о";
            //act
            string result = CrackingTechniques.CrackCaesar(input)[0];
            string res2 = CrackingTechniques.CrackCaesar(input)[1];
            //assert
            Assert.AreEqual(expected+"1", result + res2);
        }

        [TestMethod]
        public void CrackCaesarTest2()
        {
            //arrange
            string input = "//..,,.";
            string expected = "";
            //act
            string result = CrackingTechniques.CrackCaesar(input)[0];
            string res2 = CrackingTechniques.CrackCaesar(input)[1];
            //assert
            Assert.AreEqual(expected, result);
        }
    }

    [TestClass]
    public class Gronsfeld
    {
        [TestMethod]
        public void EncriptGronsfeldTest1()
        {
            //arrange
            string input = "аааа";
            string key = "321";
            string expected = "гвбг";
            //act
            string result = CryptographyTechniques.EncriptGronsfeld(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptGronsfeldTest2()
        {
            //arrange
            string input = "//..,,.";
            string key = "336";
            string expected = "";
            //act
            string result = CryptographyTechniques.EncriptGronsfeld(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptGronsfeldTest3()
        {
            //arrange
            string input = "выаь";
            string key = "кыв";
            string expected = null;
            //act
            string result = CryptographyTechniques.EncriptGronsfeld(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptGronsfeldTest4()
        {
            //arrange
            string input = "";
            string key = "";
            string expected = "";
            //act
            string result = CryptographyTechniques.EncriptGronsfeld(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptGronsfeldTest1()
        {
            //arrange
            string input = "гвбг";
            string key = "321";
            string expected = "аааа";
            //act
            string result = CryptographyTechniques.DecriptGronsfeld(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptGronsfeldTest2()
        {
            //arrange
            string input = "//..,,.";
            string key = "1";
            string expected = "";
            //act
            string result = CryptographyTechniques.DecriptGronsfeld(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptGronsfeldTest3()
        {
            //arrange
            string input = "выаь";
            string key = "к";
            string expected = null;
            //act
            string result = CryptographyTechniques.DecriptGronsfeld(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptGronsfeldTest4()
        {
            //arrange
            string input = "";
            string key = "";
            string expected = "";
            //act
            string result = CryptographyTechniques.DecriptGronsfeld(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetKeyLengthGronsfeldTest1()
        {
            //arrange
            string input = "иравылтмтыамдарирыао";
            int[] expected = {0,2,1,18,17};
            //act
            int[] result = CrackingTechniques.GetKeyLengthGronsfeld(input);
            //assert
            Assert.AreEqual(expected[0], result[0]);
            Assert.AreEqual(expected[1], result[1]);
            Assert.AreEqual(expected[2], result[2]);
            Assert.AreEqual(expected[3], result[3]);
            Assert.AreEqual(expected[4], result[4]);
        }

        [TestMethod]
        public void ReplaceGronsfeldTest1()
        {
            //arrange
            string input =  "иравыатмтыамдарирыао";
            string expected="ирКвыКтмтыамдарирыао";
            //act
            string result = CrackingTechniques.ReplaceGronsfeld(input,2,3,'а','К');
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TryAutovariantGronsfeldTest1()
        {
            //arrange
            string input = "иравыатмтыамдарирыао";
            string expected = "ояозйошыббоыйояояйёэ";
            //act
            string result = CrackingTechniques.TryAutovariantGronsfeld(input,3);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsRussianLetterTest1()
        {
            //arrange
            char input = 'п';
            bool expected = true;
            //act
            bool result = CrackingTechniques.IsRussianLetter(input);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsRussianLetterTest2()
        {
            //arrange
            char input = '4';
            bool expected = false;
            //act
            bool result = CrackingTechniques.IsRussianLetter(input);
            //assert
            Assert.AreEqual(expected, result);
        }
    }

    [TestClass]
    public class Multy
    {
        [TestMethod]
        public void EncriptMultyTest1()
        {
            //arrange
            string input = "аааа";
            string key = "гвб";
            string expected = "гвбг";
            //act
            string result = CryptographyTechniques.EncriptMulty(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptMultyTest2()
        {
            //arrange
            string input = "//..,,.";
            string key = "гво";
            string expected = "";
            //act
            string result = CryptographyTechniques.EncriptMulty(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptMultyTest3()
        {
            //arrange
            string input = "выаь";
            string key = "№;";
            string expected = null;
            //act
            string result = CryptographyTechniques.EncriptMulty(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptMultyTest4()
        {
            //arrange
            string input = "";
            string key = "";
            string expected = "";
            //act
            string result = CryptographyTechniques.EncriptMulty(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptMultyTest1()
        {
            //arrange
            string input = "гвбг";
            string key = "гвб";
            string expected = "аааа";
            //act
            string result = CryptographyTechniques.DecriptMulty(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptMultyTest2()
        {
            //arrange
            string input = "//..,,.";
            string key = "м";
            string expected = "";
            //act
            string result = CryptographyTechniques.DecriptMulty(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptMultyTest3()
        {
            //arrange
            string input = "выаь";
            string key = "№4";
            string expected = null;
            //act
            string result = CryptographyTechniques.DecriptMulty(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DecriptMultyTest4()
        {
            //arrange
            string input = "";
            string key = "";
            string expected = "";
            //act
            string result = CryptographyTechniques.DecriptMulty(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }
    }

    [TestClass]
    public class Gamma
    {
        [TestMethod]
        public void EncriptGammaTest1()
        {
            //arrange
            string input = "0";
            string key = "`";
            string expected = "P";
            //act
            string result = CryptographyTechniques.EncriptGamma(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptGammaTest2()
        {
            //arrange
            string input = "";
            string key = "гво";
            string expected = "";
            //act
            string result = CryptographyTechniques.EncriptGamma(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptGammaTest3()
        {
            //arrange
            string input = "выаь";
            string key = "";
            string expected = null;
            //act
            string result = CryptographyTechniques.EncriptGamma(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EncriptGammaTest4()
        {
            //arrange
            string input = "";
            string key = "";
            string expected = null;
            //act
            string result = CryptographyTechniques.EncriptGamma(input, key);
            //assert
            Assert.AreEqual(expected, result);
        }
    }
    }
