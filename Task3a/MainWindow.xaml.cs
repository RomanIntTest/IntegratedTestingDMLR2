﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//ШИФР МНОГОАЛФАВИТНОЙ ЗАМЕНЫ: ШИФРОВКА/РАСШИФРОВКА///////////////////////////////////////////////////////
/*
 Поле. Старая, покривившаяся, давно заброшенная часовенка, возле нее колодец, большие камни, когда-то бывшие, по-видимому, могильными плитами, и старая скамья. Видна дорога в усадьбу Гаева. В стороне, возвышаясь, темнеют тополи: там начинается вишневый сад. Вдали ряд телеграфных столбов, и далеко-далеко на горизонте неясно обозначается большой город, который бывает виден только в очень хорошую, ясную погоду. Скоро сядет солнце. Шарлотта, Яша и Дуняша сидят на скамье; Епиходов стоит возле и играет на гитаре; все сидят задумавшись. Шарлотта в старой фуражке; она сняла с плеч ружье и поправляет пряжку на ремне.
     */
namespace Task3a
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            BtnReverse.Content = "<--";
        }

        private void BtnEncrypt_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Поле ключа не заполнено", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            for (int i = 0; i < TbKey.Text.Length; i++)
            {
                if (!Char.IsLetter(TbKey.Text[i]))
                {
                    System.Windows.MessageBox.Show("Неверный формат ключа. Ключ должен содержать только буквы", "Ошибка");
                    return;
                }
            }

            TbOutput.Text = "";
            int[] Key = new int[TbKey.Text.Length];
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            for (int i = 0; i < TbKey.Text.Length; i++)
            {
                Key[i] = Alphabet.IndexOf(TbKey.Text[i]);
            }
            string InputText = TbInput.Text.ToLower();
            string Result = "";
            for (int i = 0; i < TbInput.Text.Length; i++)
            {
                if (((int)(InputText[i]) >= 1040 && (int)(InputText[i]) <= 1103) || (int)(InputText[i]) == 1105)
                    Result += InputText[i];
            }
            for (int i = 0; i < Result.Length; i++)
            {
                if (Alphabet.Contains(Result[i]))
                    TbOutput.Text += Alphabet[(Alphabet.IndexOf(Result[i]) - Key[i % TbKey.Text.Length] + 33) % 33];
            }
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }

        private void BtnDecrypt_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Поле ключа не заполнено", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            for (int i = 0; i < TbKey.Text.Length; i++)
            {
                if (!Char.IsLetter(TbKey.Text[i]))
                {
                    System.Windows.MessageBox.Show("Неверный формат ключа. Ключ должен содержать только буквы", "Ошибка");
                    return;
                }
            }

            TbOutput.Text = "";
            int[] Key = new int[TbKey.Text.Length];
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            for (int i = 0; i < TbKey.Text.Length; i++)
            {
                Key[i] = Alphabet.IndexOf(TbKey.Text[i]);
            }
            string InputText = TbInput.Text.ToLower();
            for (int i = 0; i < InputText.Length; i++)
            {
                if (Alphabet.Contains(InputText[i]))
                    TbOutput.Text += Alphabet[(Alphabet.IndexOf(InputText[i]) + Key[i % TbKey.Text.Length]) % 33];
            }
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }

        private void BtnReverse_Click(object sender, RoutedEventArgs e)
        {
            TbInput.Text = TbOutput.Text;
            TbOutput.Text = "";
        }
    }
}
//ШИФР МНОГОАЛФАВИТНОЙ ЗАМЕНЫ: ШИФРОВКА/РАСШИФРОВКА///////////////////////////////////////////////////////