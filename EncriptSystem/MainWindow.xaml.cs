﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EncriptSystem
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Stack<string> ChangeingStack = new Stack<string>();
        public Stack<string> ChangeingStack2 = new Stack<string>();

        public MainWindow()
        {
            InitializeComponent();
            BtnReverse.Content = "<--";
        }

        //ШИФР ЦЕЗАРЯ: ШИФРОВАНИЕ/РАСШИФРОВАНИЕ///////////////////////////////////////////////////////////////////////
        private void BtnEncryptCaesar_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0 || TbKey.Text.Length > 2)
            {
                System.Windows.MessageBox.Show("Неверная длина ключа. Ключ должен содержать 1 или 2 цифры", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            if ((TbKey.Text.Length == 1 && !Char.IsDigit(TbKey.Text[0])) || (TbKey.Text.Length == 2 && (!Char.IsDigit(TbKey.Text[0]) || !Char.IsDigit(TbKey.Text[1]))))
            {
                System.Windows.MessageBox.Show("Неверный формат ключа. Ключ должен содержать только цифры", "Ошибка");
                return;
            }
            if (Int32.Parse(TbKey.Text) > 32)
            {
                System.Windows.MessageBox.Show("Неверная величина сдвига. Ключ должен быть в диапазоне от 0 до 32", "Ошибка");
                return;
            }
            TbOutput.Text = CryptographyTechniques.EncriptCaesar(TbInput.Text, TbKey.Text);
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }

        private void BtnDecryptCaesar_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0 || TbKey.Text.Length > 2)
            {
                System.Windows.MessageBox.Show("Ошибка", "Неверная длина ключа. Ключ должен содержать 1 или 2 цифры");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            if ((TbKey.Text.Length == 1 && !Char.IsDigit(TbKey.Text[0])) || (TbKey.Text.Length == 2 && (!Char.IsDigit(TbKey.Text[0]) || !Char.IsDigit(TbKey.Text[1]))))
            {
                System.Windows.MessageBox.Show("Ошибка", "Неверный формат ключа. Ключ должен содержать только цифры");
                return;
            }
            if (Int32.Parse(TbKey.Text) > 32)
            {
                System.Windows.MessageBox.Show("Ошибка", "Неверная величина сдвига. Ключ должен быть в диапазоне от 0 до 32");
                return;
            }
            TbOutput.Text = CryptographyTechniques.DecriptCaesar(TbInput.Text, TbKey.Text);
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }

        private void BtnReverse_Click(object sender, RoutedEventArgs e)
        {
            TbInput.Text = TbOutput.Text;
            TbOutput.Text = "";
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //ШИФР ЦЕЗАРЯ: ВЗЛОМ//////////////////////////////////////////////////////////////////////////////////////////
        private void BtnCrack_Click(object sender, RoutedEventArgs e)
        {
            string[] Result = CrackingTechniques.CrackCaesar(TbInputCaesar.Text);
            TbOutputCaesar.Text = Result[0];
            if (TbOutputCaesar.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
            LbKeyCaesar.Content = Result[1];
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //ШИФР ГРОНСФЕЛЬДА: ШИФРОВАНИЕ/РАСШИФРОВАНИЕ//////////////////////////////////////////////////////////////////
        private void BtnEncryptGronsfeld_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Поле ключа не заполнено", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            for (int i = 0; i < TbKey.Text.Length; i++)
            {
                if (!Char.IsDigit(TbKey.Text[i]))
                {
                    System.Windows.MessageBox.Show("Неверный формат ключа. Ключ должен содержать только цифры", "Ошибка");
                    return;
                }
            }

            TbOutput.Text = CryptographyTechniques.EncriptGronsfeld(TbInput.Text, TbKey.Text);
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }

        private void BtnDecryptGronsfeld_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Поле ключа не заполнено", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            for (int i = 0; i < TbKey.Text.Length; i++)
            {
                if (!Char.IsDigit(TbKey.Text[i]))
                {
                    System.Windows.MessageBox.Show("Неверный формат ключа. Ключ должен содержать только цифры", "Ошибка");
                    return;
                }
            }

            TbOutput.Text = CryptographyTechniques.DecriptGronsfeld(TbInput.Text, TbKey.Text);
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //ШИФР ГРОНСФЕЛЬДА: ВЗЛОМ/////////////////////////////////////////////////////////////////////////////////////
        private void BtnGetKeysLengths_Click(object sender, RoutedEventArgs e)//предположить длину ключа
        {
            LbMostProbableKeysLengthGronsfeld.Content = "Наиболее вероятные длины ключа:\n";
            int[] Result = CrackingTechniques.GetKeyLengthGronsfeld(TbInputGronsfeld.Text);
            for (int i = 0; i < 5; i++)
            {
                LbMostProbableKeysLengthGronsfeld.Content += Result[i].ToString() + "\n";
            }
        }

        private void BtnTry_Click(object sender, RoutedEventArgs e)
        {
            TbAutoResultGronsfeld.Text = CrackingTechniques.TryAutovariantGronsfeld(TbInputGronsfeld.Text, int.Parse(TbKeyLengthGronsfeld.Text));//new string(Result);
        }

        private void BtnReplace_Click(object sender, RoutedEventArgs e)
        {
            if (TbWithThatGronsfeld.Text.Length != 1 || TbWhatGronsfeld.Text.Length != 1)
            {
                System.Windows.MessageBox.Show("В полях должно быть по одному символу");
                return;
            }
            if (!CrackingTechniques.IsRussianLetter(TbWithThatGronsfeld.Text[0]) || !CrackingTechniques.IsRussianLetter(TbWhatGronsfeld.Text[0]))
            {
                System.Windows.MessageBox.Show("Символ не является русской буквой");
                return;
            }

            int KeyLen = Int32.Parse(TbKeyLengthGronsfeld.Text);
            string text = "";
            if (TbOutputGronsfeld.Text == "")
                text = TbAutoResultGronsfeld.Text;
            else
                text = TbOutputGronsfeld.Text;
            TbOutputGronsfeld.Text = CrackingTechniques.ReplaceGronsfeld(TbAutoResultGronsfeld.Text,int.Parse(TbPartGronsfeld.Text),int.Parse(TbKeyLengthGronsfeld.Text),TbWhatGronsfeld.Text[0], TbWithThatGronsfeld.Text[0]);

            TbWhatGronsfeld.Text = "Что";
            TbWithThatGronsfeld.Text = "Чем";
            ChangeingStack.Push(TbPartGronsfeld.Text + "|" + TbWhatGronsfeld.Text.ToString() + "|" + TbWithThatGronsfeld.Text.ToString());
        }

        private void TbWithThat_MouseEnter(object sender, MouseEventArgs e)
        {
            if (TbWithThatGronsfeld.Text == "Чем")
                TbWithThatGronsfeld.Text = "";
        }

        private void TbWithThat_MouseLeave(object sender, MouseEventArgs e)
        {
            if (TbWithThatGronsfeld.Text == "")
                TbWithThatGronsfeld.Text = "Чем";
        }

        private void TbWhat_MouseEnter(object sender, MouseEventArgs e)
        {
            if (TbWhatGronsfeld.Text == "Что")
                TbWhatGronsfeld.Text = "";
        }

        private void TbWhat_MouseLeave(object sender, MouseEventArgs e)
        {
            if (TbWhatGronsfeld.Text == "")
                TbWhatGronsfeld.Text = "Что";
        }

        private void RemoveLastAction_Click(object sender, RoutedEventArgs e)
        {
            if (ChangeingStack.Count == 0)
                return;
            string Symbols = ChangeingStack.Pop();
            TbOutputGronsfeld.Text = TbOutputGronsfeld.Text.Replace(Symbols[1], Symbols[0]);
        }

        private void TbPart_MouseLeave(object sender, MouseEventArgs e)
        {
            if (TbPartGronsfeld.Text == "")
                TbPartGronsfeld.Text = "В какой части?";
        }

        private void TbPart_MouseEnter(object sender, MouseEventArgs e)
        {
            if (TbPartGronsfeld.Text == "В какой части?")
                TbPartGronsfeld.Text = "";
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //ШИФР МНОГОАЛФАВИТНОЙ ЗАМЕНЫ: ШИФРОВАНИЕ/РАСШИФРОВАНИЕ///////////////////////////////////////////////////////
        private void BtnEncryptMulty_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Поле ключа не заполнено", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            for (int i = 0; i < TbKey.Text.Length; i++)
            {
                if (!Char.IsLetter(TbKey.Text[i]))
                {
                    System.Windows.MessageBox.Show("Неверный формат ключа. Ключ должен содержать только буквы", "Ошибка");
                    return;
                }
            }
            TbOutput.Text = CryptographyTechniques.EncriptMulty(TbInput.Text, TbKey.Text);
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }

        private void BtnDecryptMulty_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Поле ключа не заполнено", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }
            for (int i = 0; i < TbKey.Text.Length; i++)
            {
                if (!Char.IsLetter(TbKey.Text[i]))
                {
                    System.Windows.MessageBox.Show("Неверный формат ключа. Ключ должен содержать только буквы", "Ошибка");
                    return;
                }
            }

            TbOutput.Text = CryptographyTechniques.DecriptMulty(TbInput.Text, TbKey.Text);
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //ГАММА-ШИФР: ШИФРОВАНИЕ/РАСШИФРОВАНИЕ////////////////////////////////////////////////////////////////////////
        private void BtnEncryptGamma_Click(object sender, RoutedEventArgs e)
        {
            if (TbKey.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Поле ключа не заполнено", "Ошибка");
                return;
            }
            if (TbInput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("В поле входного текста пусто", "Ошибка");
                return;
            }

            TbOutput.Text = CryptographyTechniques.EncriptGamma(TbInput.Text, TbKey.Text);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    public static class CryptographyTechniques
    {
        public static string EncriptCaesar(string Input, string Key)
        {
            int Shift;
            if (!Int32.TryParse(Key, out Shift))
            {
                return null;
            }
            Shift = Int32.Parse(Key);
            string Result = "";
            Input = Input.ToLower();
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            for (int i = 0; i < Input.Length; i++)
            {
                if (Alphabet.Contains(Input[i]))
                    Result += Alphabet[(Alphabet.IndexOf(Input[i]) + Shift) % 33];
            }
            return Result;
        }

        public static string DecriptCaesar(string Input, string Key)
        {
            int Shift;
            if (!Int32.TryParse(Key, out Shift))
            {
                return null;
            }
            Shift = Int32.Parse(Key);
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            Input = Input.ToLower();
            string Result = "";
            for (int i = 0; i < Input.Length; i++)
            {
                if (Alphabet.Contains(Input[i]))
                    Result += Alphabet[(Alphabet.IndexOf(Input[i]) - Shift + 33) % 33];
            }
            return Result;
        }

        public static string EncriptGronsfeld (string Input, string key)
        {
            int[] Key = new int[key.Length];
            for (int i = 0; i<key.Length; i++)
            {
                Key[i] = (int)(key[i]) - 48;
                if (Key[i]>9)
                    return null;
            }
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            Input = Input.ToLower();
            string Result = "", RetVal = "";
            for (int i = 0; i<Input.Length; i++)
            {
                if (((int)(Input[i]) >= 1040 && (int)(Input[i]) <= 1103) || (int)(Input[i]) == 1105)
                    Result += Input[i];
            }
            for (int i = 0; i<Result.Length; i++)
            {
                if (Alphabet.Contains(Result[i]))
                    RetVal += Alphabet[(Alphabet.IndexOf(Result[i]) + Key[i % key.Length]) % 33];
            }
            return RetVal;
        }

        public static string DecriptGronsfeld (string Input, string key)
        {
            int[] Key = new int[key.Length];
            for (int i = 0; i < key.Length; i++)
            {
                Key[i] = (int)(key[i]) - 48;
                if (Key[i] > 9)
                    return null;
            }
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя", RetVal = "";
            Input = Input.ToLower();
            for (int i = 0; i < Input.Length; i++)
            {
                if (Alphabet.Contains(Input[i]))
                    RetVal += Alphabet[(Alphabet.IndexOf(Input[i]) - Key[i % key.Length] + 33) % 33];
            }
            return RetVal;
        }

        public static string EncriptMulty(string Input, string key)
        {
            string RetVal = "";
            int[] Key = new int[key.Length];
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            for (int i = 0; i < key.Length; i++)
            {
                Key[i] = Alphabet.IndexOf(key[i]);
                if (Key[i] == -1)
                    return null;
            }
            Input = Input.ToLower();
            string Result = "";
            for (int i = 0; i < Input.Length; i++)
            {
                if (((int)(Input[i]) >= 1040 && (int)(Input[i]) <= 1103) || (int)(Input[i]) == 1105)
                    Result += Input[i];
            }
            for (int i = 0; i < Result.Length; i++)
            {
                if (Alphabet.Contains(Result[i]))
                    RetVal += Alphabet[(Alphabet.IndexOf(Result[i]) + Key[i % key.Length] + 33) % 33];
            }
            return RetVal;
        }

        public static string DecriptMulty(string Input, string key)
        {
            string RetVal = "";
            int[] Key = new int[key.Length];
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            for (int i = 0; i < key.Length; i++)
            {
                Key[i] = Alphabet.IndexOf(key[i]);
                if (Key[i] == -1)
                    return null;
            }
            Input = Input.ToLower();
            for (int i = 0; i < Input.Length; i++)
            {
                if (Alphabet.Contains(Input[i]))
                    RetVal += Alphabet[(Alphabet.IndexOf(Input[i]) - Key[i % key.Length] + 33) % 33];
            }
            return RetVal;
        }

        public static string EncriptGamma(string Input, string key)
        {
            if (key.Length == 0)
                return null;
            string RetVal = "";
            for (int i = 0; i < Input.Length; i++)
            {
                RetVal += (char)((char)(key[i % key.Length]) ^ (char)(Input[i]));
            }
            return RetVal;
        }
    }

    public class CrackingTechniques
    {
        static List<double> AlphabetFrequency = new List<double>();
        static List<char> AlphabetOfFrequency = new List<char>();

        static CrackingTechniques()
        {
            //гронсфельд взлом
            AlphabetOfFrequency.Add('о');
            AlphabetOfFrequency.Add('е');
            AlphabetOfFrequency.Add('а');
            AlphabetOfFrequency.Add('и');
            AlphabetOfFrequency.Add('н');
            AlphabetOfFrequency.Add('т');
            AlphabetOfFrequency.Add('с');
            AlphabetOfFrequency.Add('р');
            AlphabetOfFrequency.Add('в');
            AlphabetOfFrequency.Add('л');
            AlphabetOfFrequency.Add('к');
            AlphabetOfFrequency.Add('м');
            AlphabetOfFrequency.Add('д');
            AlphabetOfFrequency.Add('п');
            AlphabetOfFrequency.Add('у');
            AlphabetOfFrequency.Add('я');
            AlphabetOfFrequency.Add('ы');
            AlphabetOfFrequency.Add('ь');
            AlphabetOfFrequency.Add('г');
            AlphabetOfFrequency.Add('з');
            AlphabetOfFrequency.Add('б');
            AlphabetOfFrequency.Add('ч');
            AlphabetOfFrequency.Add('й');
            AlphabetOfFrequency.Add('х');
            AlphabetOfFrequency.Add('ж');
            AlphabetOfFrequency.Add('ш');
            AlphabetOfFrequency.Add('ю');
            AlphabetOfFrequency.Add('ц');
            AlphabetOfFrequency.Add('щ');
            AlphabetOfFrequency.Add('э');
            AlphabetOfFrequency.Add('ф');
            AlphabetOfFrequency.Add('ъ');
            AlphabetOfFrequency.Add('ё');

            AlphabetFrequency.Add(10.983);
            AlphabetFrequency.Add(8.483);
            AlphabetFrequency.Add(7.998);
            AlphabetFrequency.Add(7.367);
            AlphabetFrequency.Add(6.700);
            AlphabetFrequency.Add(6.318);
            AlphabetFrequency.Add(5.473);
            AlphabetFrequency.Add(4.746);
            AlphabetFrequency.Add(4.533);
            AlphabetFrequency.Add(4.343);
            AlphabetFrequency.Add(3.486);
            AlphabetFrequency.Add(3.203);
            AlphabetFrequency.Add(2.977);
            AlphabetFrequency.Add(2.804);
            AlphabetFrequency.Add(2.615);
            AlphabetFrequency.Add(2.001);
            AlphabetFrequency.Add(1.898);
            AlphabetFrequency.Add(1.735);
            AlphabetFrequency.Add(1.687);
            AlphabetFrequency.Add(1.641);
            AlphabetFrequency.Add(1.592);
            AlphabetFrequency.Add(1.450);
            AlphabetFrequency.Add(1.208);
            AlphabetFrequency.Add(0.966);
            AlphabetFrequency.Add(0.940);
            AlphabetFrequency.Add(0.718);
            AlphabetFrequency.Add(0.638);
            AlphabetFrequency.Add(0.486);
            AlphabetFrequency.Add(0.361);
            AlphabetFrequency.Add(0.331);
            AlphabetFrequency.Add(0.267);
            AlphabetFrequency.Add(0.037);
            AlphabetFrequency.Add(0.013);
        }

        public static string[] CrackCaesar(string Input)
        {
            string[] RetVal = new string[2];
            RetVal[0] = "";
            List<double> UserFrequency = new List<double>();
            for (int i = 0; i < 33; i++)
            {
                UserFrequency.Add(0);
            }
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            for (int i = 0; i < Input.Length; i++)//ищем частоту
            {
                if (Alphabet.Contains(Input[i]))
                    UserFrequency[Alphabet.IndexOf(Input[i])] += 1;
            }
            int Shift = 15 > UserFrequency.IndexOf(UserFrequency.Max()) ? 15 - UserFrequency.IndexOf(UserFrequency.Max()) : UserFrequency.IndexOf(UserFrequency.Max()) - 15;
            for (int i = 0; i < Input.Length; i++)
            {
                if (Alphabet.Contains(Input[i]))
                    RetVal[0] += Alphabet[(Alphabet.IndexOf(Input[i]) - Shift + 33) % 33];
            }
            RetVal[1] = Shift.ToString();
            return RetVal;
        }

        public static int[] GetKeyLengthGronsfeld(string Input)
        {
            int[] RetVal = new int[5];
            string lower = Input.ToLower();
            string text = "";
            for (int i = 0; i < Input.Length; i++)//Очистка текста от лишних букв
                if (Input[i] == 1105 || (Input[i] <= 1103 && Input[i] >= 1040))
                    text += Input[i];
            string OnePart = "";
            List<double> FreqencyDeviation = new List<double>();
            for (int KeyLen = 1; KeyLen <= 20; KeyLen++)//идём по длинам ключей
            {
                List<double> CurrentDeviations = new List<double>();
                OnePart = "";//здесь хранится часть текста
                for (int i = 0; i < KeyLen; i++)//идём по каждой части ключа, собираем длина_ключа частей текста
                {
                    int j = 0;
                    while (j * KeyLen + i < text.Length)//перебираем весь текст
                    {
                        OnePart += text[j * KeyLen + i];
                        j++;
                    }
                    string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
                    double[] FrequencyMas = new double[33];
                    for (int t = 0; t < 33; t++)//массив частот текушей части текста
                        FrequencyMas[t] = 0;

                    for (int t = 0; t < OnePart.Length; t++)
                        FrequencyMas[Alphabet.IndexOf(OnePart[t])]++;

                    for (int t = 0; t < 33; t++)
                        FrequencyMas[t] /= OnePart.Length / 100.0;
                    Array.Sort(FrequencyMas);
                    Array.Reverse(FrequencyMas);//теперь частоты отсортированы по величине, чтобы сравнивать с алфавитными
                    double Sigma = 0;//величина отклонения от алфавита
                    for (int t = 0; t < 33; t++)
                        Sigma += Math.Abs(AlphabetFrequency[t] - FrequencyMas[t]);//вычисляем суммарную погрешность
                    CurrentDeviations.Add(Sigma);
                }
                double average = 0;//среднее отклонение при данной длине ключа
                foreach (double x in CurrentDeviations)
                    average += x;
                average /= CurrentDeviations.Count;
                FreqencyDeviation.Add(average);
            }

            for (int i = 0; i < 5; i++)
            {
                RetVal[i] = FreqencyDeviation.IndexOf(FreqencyDeviation.Min());
                FreqencyDeviation[FreqencyDeviation.IndexOf(FreqencyDeviation.Min())] = 500;
            }
            return RetVal;
        }

        public static string TryAutovariantGronsfeld(string Input, int key)
        {
            string RetVal = "";
            string lower = Input.ToLower();
            string text = "";
            for (int i = 0; i < Input.Length; i++)
                if (Input[i] == 1105 || (Input[i] <= 1103 && Input[i] >= 1040))//выбираем русские буквы
                    text += Input[i];
            string OnePart = "";//здесь хранится часть текста
            char[] Result = new char[text.Length];
            for (int i = 0; i < key; i++)//идём по каждой части ключа
            {
                OnePart = "";
                int j = 0;
                while (j * key + i < text.Length)//перебираем весь текст
                {
                    OnePart += text[j * key + i];
                    j++;
                }
                string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
                double[] FrequencyMas = new double[33];
                for (int t = 0; t < 33; t++)
                    FrequencyMas[t] = 0;

                for (int t = 0; t < OnePart.Length; t++)
                    FrequencyMas[Alphabet.IndexOf(OnePart[t])]++;

                for (int t = 0; t < 33; t++)
                    FrequencyMas[t] /= OnePart.Length / 100.0;//посчитали частоту части текста
                char FreqOfMas = Alphabet[IndexOfMax(FrequencyMas)];
                int shift = Alphabet.IndexOf(FreqOfMas) - Alphabet.IndexOf('о');
                for (int t = 0; t < OnePart.Length; t++)
                {
                    Result[t * key + i] = Alphabet[(Alphabet.IndexOf(OnePart[t]) - shift + 33) % 33];
                }
            }
            RetVal = new string(Result);
            return RetVal;
        }

        public static string ReplaceGronsfeld(string Input, int Part, int keyLen, char What, char WithThat)
        {
            string OnePart = "";
            string RetVal = "";
            int j = 0;
            while (j * keyLen + Part < Input.Length)//перебираем весь текст
            {
                OnePart += Input[j * keyLen + Part];
                j++;
            }
            What = (What.ToString().ToLower()).ToCharArray()[0];
            WithThat = (WithThat.ToString().ToUpper()).ToCharArray()[0];
            string Text = OnePart;
            Text = Text.Replace(What, WithThat);
            

            string Result = "";
            int t = 0;
            for (int i = 0; i < Input.Length; i++)
            {
                if (i % keyLen == Part)
                {
                    Result += Text[t];
                    t++;
                }
                else
                    Result += Input[i];
            }
            RetVal = Result;
            return RetVal;
        }

        public static bool IsRussianLetter(char letter)
        {
            if ((letter >= 1040 && letter <= 1103) || letter == 1105)
                return true;
            return false;
        }

        private static int IndexOfMax(double[] mas)
        {
            double Max = 0;
            int Result = 0;
            for (int i = 0; i < mas.Count(); i++)
                if (mas[i] > Max)
                {
                    Max = mas[i];
                    Result = i;
                }
            return Result;
        }
    }
}
