﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//ШИФР ЦЕЗАРЯ: ВЗЛОМ////////////////////////////////////////////////////////////////////////////////

/*Пример
 * чцумщъзшзжчцтшрйрйазжщжлзйхцпзишцамххзжязщцймхтзйцпумхммтцуцлмюицудармтзфхртцклзъцигйармчцйрлрфцфыфцкрудхгфрчуръзфррщъзшзжщтзфджйрлхзлцшцкзйыщзлдиыкзмйзйщъцшцхмйцпйгазжщдъмфхмёъъцчцуръзфхзярхзмъщжйрахмйгсщзлйлзуршжлъмумкшзьхгэщъцуицйрлзумтцлзумтцхзкцшрпцхъмхмжщхццицпхзязмъщжицудацскцшцлтцъцшгсигйзмъйрлмхъцудтцйцямхдэцшцаыёжщхыёчцкцлыщтцшцщжлмъщцухюмазшуцъъзжазрлыхжазщрлжъхзщтзфдммчрэцлцйщъцръйцпумрркшзмъхзкръзшмйщмщрлжъпзлыфзйарщдазшуцъъзйщъзшцсьышзотмцхзщхжузщчумяшыодмрчцчшзйужмъчшжотыхзшмфхм
 */
namespace Task1b
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnCrack_Click(object sender, RoutedEventArgs e)
        {
            List<double> UserFrequency = new List<double>();
            for (int i = 0; i<33; i++)
            {
                UserFrequency.Add(0);
            }
            string InputText = TbInput.Text;
            string Alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            for (int i = 0; i < InputText.Length; i++)//ищем частоту
            {
                if (Alphabet.Contains(InputText[i]))
                    UserFrequency[Alphabet.IndexOf(InputText[i])] += 1;
            }
            int Shift = 15 > UserFrequency.IndexOf(UserFrequency.Max()) ? 15 - UserFrequency.IndexOf(UserFrequency.Max()) : UserFrequency.IndexOf(UserFrequency.Max()) - 15;
            for (int i = 0; i < InputText.Length; i++)
            {
                if (Alphabet.Contains(InputText[i]))
                    TbOutput.Text += Alphabet[(Alphabet.IndexOf(InputText[i]) - Shift + 33) % 33];
            }
            if (TbOutput.Text.Length == 0)
            {
                System.Windows.MessageBox.Show("Не встречено ни одного символа из русского алфавита", "Ошибка");
                return;
            }
            LbKey.Content = Shift.ToString();
        }
    }
}
//ШИФР ЦЕЗАРЯ: ВЗЛОМ////////////////////////////////////////////////////////////////////////////////
